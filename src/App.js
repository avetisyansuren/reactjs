import React, { Component } from 'react';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'
import {browserHistory} from 'react-router'
import {Provider} from 'react-redux'
import {Router, Route} from 'react-router'

import reducers from 'reducers'
import Layout from 'containers/layout'
import Products from 'containers/products'
import Product from 'containers/product'
import Basket from 'containers/basket'


const store = createStore(reducers, composeWithDevTools(
    applyMiddleware(thunk)
));

class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <Router history={browserHistory}>
                <Route component={Layout}>
                  <Route path='/' component={Products}/>
                </Route>
                <Route path='/products/:id' component={Product} />
                <Route path='/basket' component={Basket} />

            </Router>
        </Provider>
    );
  }
}

export default App;
